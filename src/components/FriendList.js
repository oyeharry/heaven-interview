import React, { Component, PropTypes } from 'react';
import styles from './FriendList.css';
import FriendListItem from './FriendListItem';
import Pagination from './Pagination';

class FriendList extends Component {
  onPrevPageSelect = () => {
    const { curPage } = this.props;
    if (curPage > 1) {
      this.updatePage(curPage - 1);
    }
  };
  onNextPageSelect = () => {
    const { curPage, totalPage } = this.props;
    if (curPage < totalPage) {
      this.updatePage(curPage + 1);
    }
  };
  onPageSelect = curPage => {
    this.updatePage(curPage);
  };
  updatePage(curPage) {
    const {
      actions: { changeFriendsPage }
    } = this.props;
    changeFriendsPage(curPage);
  }
  render() {
    const { curPage, totalPage, friends, actions } = this.props;

    return (
      <div>
        <ul className={styles.friendList}>
          {friends.map(friend => {
            const { id, name, sex, starred } = friend;
            return (
              <FriendListItem
                key={id}
                id={id}
                name={name}
                sex={sex}
                starred={starred}
                {...actions}
              />
            );
          })}
        </ul>
        {totalPage > 1 ? (
          <Pagination
            totalPage={totalPage}
            activePage={curPage}
            onPrevPageSelect={this.onPrevPageSelect}
            onNextPageSelect={this.onNextPageSelect}
            onPageSelect={this.onPageSelect}
          />
        ) : null}
      </div>
    );
  }
}

FriendList.propTypes = {
  friends: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

export default FriendList;
