import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './FriendListItem.css';

class FriendListItem extends Component {
  render() {
    const { name, id, sex, starred, starFriend, deleteFriend } = this.props;
    return (
      <li className={styles.friendListItem}>
        <div className={styles.friendInfos}>
          <div>
            <span>{name}</span>
          </div>
          <div>
            <span>{sex}</span>
          </div>
          <div>
            <small>xx friends in common</small>
          </div>
        </div>
        <div className={styles.friendActions}>
          <button
            data-testid="starFriendTestBtn"
            className={`btn btn-default ${styles.btnAction}`}
            onClick={() => starFriend(id)}
          >
            <i
              className={classnames('fa', {
                'fa-star': starred,
                'fa-star-o': !starred
              })}
            />
          </button>
          <button
            data-testid="deleteFriendTestBtn"
            className={`btn btn-default ${styles.btnAction}`}
            onClick={() => deleteFriend(id)}
          >
            <i className="fa fa-trash" />
          </button>
        </div>
      </li>
    );
  }
}

FriendListItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  starred: PropTypes.bool,
  starFriend: PropTypes.func.isRequired
};

export default FriendListItem;
