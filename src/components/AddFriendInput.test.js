import React from 'react';

import { render, fireEvent, cleanup } from 'react-testing-library';

import AddFriendInput from './AddFriendInput';

afterEach(cleanup);

it('Should verify addFriend fire onSubmit', () => {
  const addFriend = jest.fn();
  const { getByTestId, container } = render(
    <AddFriendInput addFriend={addFriend} />
  );
  const addFriendForm = getByTestId('addFriendForm');
  const inputElm = container.querySelector('input');
  inputElm.value = 'John';
  fireEvent.submit(addFriendForm);
  expect(addFriend).toHaveBeenCalledTimes(1);
});

it('Should verify addFriend fire onSubmit with correct args', () => {
  const addFriend = jest.fn();
  const name = 'John';
  const sex = 'Female';
  const { getByTestId, container } = render(
    <AddFriendInput addFriend={addFriend} />
  );
  const addFriendForm = getByTestId('addFriendForm');
  const inputElm = container.querySelector('input');
  const selectElm = container.querySelector('select');
  inputElm.value = name;
  selectElm.value = sex;
  fireEvent.submit(addFriendForm);
  expect(addFriend).toHaveBeenCalledWith(name, sex);
});
