import React from 'react';
import classnames from 'classnames';

import styles from './Pagination.css';

function Pagination(props) {
  const {
    totalPage,
    onPrevPageSelect,
    activePage,
    onNextPageSelect,
    onPageSelect
  } = props;

  const numElements = [];
  for (let i = 1; i <= totalPage; i++) {
    const activeClass = classnames({ active: i === activePage });
    numElements.push(
      <li key={i} className={activeClass}>
        <a onClick={() => onPageSelect(i)}>{i}</a>
      </li>
    );
  }

  return (
    <nav aria-label="Page navigation" className={styles.paginationContainer}>
      <ul className={classnames('pagination pagination-sm', styles.pagination)}>
        <li className={classnames({ disabled: activePage === 1 })}>
          <a onClick={onPrevPageSelect} aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        {numElements}
        <li className={classnames({ disabled: activePage === totalPage })}>
          <a onClick={onNextPageSelect} aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  );
}

export default Pagination;
