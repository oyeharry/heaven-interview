import React from 'react';

import { render, fireEvent, cleanup } from 'react-testing-library';

import Pagination from './Pagination';

afterEach(cleanup);

it('Should verify pagination render and page select', () => {
  const onPageSelect = jest.fn();
  const totalPage = 5;
  const curPage = 2;
  const { getByText } = render(
    <Pagination
      totalPage={totalPage}
      activePage={curPage}
      onPageSelect={onPageSelect}
    />
  );
  const num3Elem = getByText('3');
  fireEvent.click(num3Elem);
  expect(onPageSelect).toHaveBeenCalledWith(3);
});

it('Should verify pagination next button click', () => {
  const onNextPageSelect = jest.fn();
  const totalPage = 5;
  const curPage = 2;
  const { getByText } = render(
    <Pagination
      totalPage={totalPage}
      activePage={curPage}
      onNextPageSelect={onNextPageSelect}
    />
  );
  const nextElem = getByText('»');
  fireEvent.click(nextElem);
  expect(onNextPageSelect).toHaveBeenCalledTimes(1);
});

it('Should verify pagination prev button click', () => {
  const onPrevPageSelect = jest.fn();
  const totalPage = 5;
  const curPage = 2;
  const { getByText } = render(
    <Pagination
      totalPage={totalPage}
      activePage={curPage}
      onPrevPageSelect={onPrevPageSelect}
    />
  );
  const prevElem = getByText('«');
  fireEvent.click(prevElem);
  expect(onPrevPageSelect).toHaveBeenCalledTimes(1);
});
