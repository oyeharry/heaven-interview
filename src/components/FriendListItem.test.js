import React from 'react';

import { render, fireEvent, cleanup } from 'react-testing-library';

import FriendListItem from './FriendListItem';

afterEach(cleanup);

it('Should render item', () => {
  const friendItem = {
    id: 1,
    name: 'John',
    sex: 'Male',
    starred: true,
    starFriend: jest.fn()
  };
  const { queryByText } = render(<FriendListItem {...friendItem} />);
  const nameElem = queryByText(friendItem.name);
  const sexElem = queryByText(friendItem.sex);
  expect(nameElem).toBeTruthy();
  expect(sexElem).toBeTruthy();
});

it('Should star item', () => {
  const friendItem = {
    id: 3,
    name: 'John',
    sex: 'Male',
    starred: true,
    starFriend: jest.fn()
  };
  const { getByTestId } = render(<FriendListItem {...friendItem} />);
  const starButton = getByTestId('starFriendTestBtn');
  fireEvent.click(starButton);
  expect(friendItem.starFriend).toHaveBeenCalledWith(friendItem.id);
});

it('Should star item', () => {
  const friendItem = {
    id: 3,
    name: 'John',
    sex: 'Male',
    starred: true,
    starFriend: jest.fn(),
    deleteFriend: jest.fn()
  };
  const { getByTestId } = render(<FriendListItem {...friendItem} />);
  const starButton = getByTestId('deleteFriendTestBtn');
  fireEvent.click(starButton);
  expect(friendItem.deleteFriend).toHaveBeenCalledWith(friendItem.id);
});
