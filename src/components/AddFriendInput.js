import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './AddFriendInput.css';

class AddFriendInput extends Component {
  render() {
    return (
      <form onSubmit={this.handleSubmit} data-testid="addFriendForm">
        <select className="form-control" name="sex">
          <option value="Male">Male</option>
          <option value="Female">Female</option>
        </select>
        <input
          name="name"
          type="text"
          autoComplete="off"
          autoFocus="true"
          className={classnames('form-control', styles.addFriendInput)}
          placeholder="Type the name of a friend"
        />
      </form>
    );
  }

  handleSubmit = e => {
    e.preventDefault();
    const { name, sex } = e.target.elements;
    const nameVal = name.value.trim();
    if (nameVal) {
      this.props.addFriend(nameVal, sex.value);
      e.target.reset();
    }
  };
}

AddFriendInput.propTypes = {
  addFriend: PropTypes.func.isRequired
};

export default AddFriendInput;
