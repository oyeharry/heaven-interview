import React from 'react';

import { render, fireEvent, cleanup } from 'react-testing-library';

import FriendList from './FriendList';

afterEach(cleanup);

it('Should render friend list', () => {
  const friends = [
    {
      id: 1,
      name: 'John',
      sex: 'Male',
      starred: true
    },
    {
      id: 2,
      name: 'Marry',
      sex: 'Female',
      starred: true
    }
  ];
  const { queryByText } = render(
    <FriendList actions={{ starFriend: () => {} }} friends={friends} />
  );
  const nameElem = queryByText(friends[1].name);
  const sexElem = queryByText(friends[1].sex);
  expect(nameElem).toBeTruthy();
  expect(sexElem).toBeTruthy();
});

it('Should not show the pagination', () => {
  const { queryByText } = render(
    <FriendList totalPage={1} friends={[]} actions={{}} />
  );
  const numElem = queryByText('1', { exact: true });
  expect(numElem).toBeNull();
});

it('Should call function with correct number for page number', () => {
  const actions = {
    changeFriendsPage: jest.fn()
  };
  const { getByText } = render(
    <FriendList totalPage={5} actions={actions} friends={[]} />
  );
  const numElem = getByText('2');
  fireEvent.click(numElem);
  expect(actions.changeFriendsPage).toHaveBeenCalledWith(2);
});

it('Should call function with correct number for next button', () => {
  const actions = {
    changeFriendsPage: jest.fn()
  };
  const { getByText } = render(
    <FriendList totalPage={5} curPage={2} actions={actions} friends={[]} />
  );
  const nextElem = getByText('»');
  fireEvent.click(nextElem);
  expect(actions.changeFriendsPage).toHaveBeenCalledWith(3);
});

it('Should call function with correct number for prev button', () => {
  const actions = {
    changeFriendsPage: jest.fn()
  };
  const { getByText } = render(
    <FriendList totalPage={5} curPage={2} actions={actions} friends={[]} />
  );
  const prevElem = getByText('«');
  fireEvent.click(prevElem);
  expect(actions.changeFriendsPage).toHaveBeenCalledWith(1);
});
