import * as types from '../constants/ActionTypes';

import {
  addFriend,
  deleteFriend,
  starFriend,
  changeFriendsPage
} from './FriendsActions';

it('addFriend should return valid object', () => {
  const name = 'John';
  const sex = 'Male';
  expect(addFriend(name, sex)).toMatchObject({
    type: types.ADD_FRIEND,
    name,
    sex
  });
});

it('deleteFriend should return valid object', () => {
  const id = 1;
  expect(deleteFriend(id)).toMatchObject({
    type: types.DELETE_FRIEND,
    id
  });
});

it('starFriend should return valid object', () => {
  const id = 1;
  expect(starFriend(id)).toMatchObject({
    type: types.STAR_FRIEND,
    id
  });
});

it('changeFriendsPage should return valid object', () => {
  const curPage = 3;
  expect(changeFriendsPage(curPage)).toMatchObject({
    type: types.CHANGE_FRIENDS_PAGE,
    curPage
  });
});

it('changeFriendsPage should return valid object with pageSize', () => {
  const curPage = 3;
  expect(changeFriendsPage(curPage)).toMatchObject({
    type: types.CHANGE_FRIENDS_PAGE,
    curPage
  });
});
