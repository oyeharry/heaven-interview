import * as types from '../constants/ActionTypes';

const initialState = {
  pageSize: 2,
  curPage: 1,
  curPageFriendsById: [],
  friendsById: [
    {
      id: 1,
      name: 'Theodore Roosevelt',
      sex: 'Male',
      starred: true
    },
    {
      id: 2,
      name: 'Abraham Lincoln',
      sex: 'Male',
      starred: false
    },
    {
      id: 3,
      name: 'George Washington',
      sex: 'Male',
      starred: false
    }
  ]
};

function getPaginationState(items, curPage, pageSize) {
  const totalPage = Math.ceil(items.length / pageSize);

  if (totalPage && curPage > totalPage) {
    curPage = totalPage;
  }

  const startIndex = (curPage - 1) * pageSize;
  let endIndex = curPage * pageSize;
  endIndex = endIndex > items.length ? items.length : endIndex;
  const curPageItems = items.slice(startIndex, endIndex);

  return {
    curPage,
    curPageItems,
    totalPage
  };
}

const { friendsById, curPage, pageSize } = initialState;
const { curPageItems, totalPage } = getPaginationState(
  friendsById,
  curPage,
  pageSize
);
initialState.curPageFriendsById = curPageItems;
initialState.totalPage = totalPage;

export default function friends(state = initialState, action) {
  const { type, name, sex, id, curPage } = action;
  let newState;
  switch (type) {
    case types.ADD_FRIEND:
      const ids = state.friendsById.map(item => item.id);
      const newId = Math.max.apply(Math, ids.length ? ids : [0]);
      newState = {
        ...state,
        friendsById: [
          ...state.friendsById,
          {
            id: newId + 1,
            name,
            sex
          }
        ]
      };
      break;
    case types.DELETE_FRIEND:
      newState = {
        ...state,
        friendsById: state.friendsById.filter(item => item.id !== id)
      };
      break;
    case types.STAR_FRIEND:
      let friends = [...state.friendsById];
      let friend = friends.find(item => item.id === id);
      friend.starred = !friend.starred;
      newState = {
        ...state,
        friendsById: friends
      };
      break;
    default:
      newState = state;
  }

  switch (type) {
    case types.ADD_FRIEND:
    case types.DELETE_FRIEND:
    case types.CHANGE_FRIENDS_PAGE:
      const { friendsById, pageSize } = newState;
      const curPageNum = curPage || newState.curPage;
      const pageState = getPaginationState(friendsById, curPageNum, pageSize);
      const { curPageItems: curPageFriendsById } = pageState;
      newState = { ...newState, curPageFriendsById, ...pageState };
      break;

    default:
      break;
  }

  return newState;
}
