import React, { Component } from 'react';
import styles from './FriendListApp.css';
import { connect } from 'react-redux';

import {
  addFriend,
  deleteFriend,
  starFriend,
  changeFriendsPage
} from '../actions/FriendsActions';
import { FriendList, AddFriendInput } from '../components';

class FriendListApp extends Component {
  render() {
    const {
      friendlist: { curPageFriendsById, curPage, totalPage },
      addFriend,
      deleteFriend,
      starFriend,
      changeFriendsPage
    } = this.props;

    const actions = {
      addFriend,
      deleteFriend,
      starFriend,
      changeFriendsPage
    };

    return (
      <div className={styles.friendListApp}>
        <h1>The FriendList</h1>
        <AddFriendInput addFriend={actions.addFriend} />
        <FriendList
          friends={curPageFriendsById}
          curPage={curPage}
          totalPage={totalPage}
          actions={actions}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(
  mapStateToProps,
  {
    addFriend,
    deleteFriend,
    starFriend,
    changeFriendsPage
  }
)(FriendListApp);
